package com.ms.kotlinpethouse

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.ms.kotlinpethouse.TestControllerMvcIT.TestControllerMvcITTestConfig.TestController
import com.ms.kotlinpethouse.TestControllerMvcIT.TestControllerMvcITTestConfig.TestForm
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

/**
 * Unit-tests of validations of complex forms.
 */
@ExtendWith(SpringExtension::class)
@WebMvcTest(TestController::class)
@AutoConfigureWebMvc
class TestControllerMvcIT {
    @Autowired
    private lateinit var mockMvc: MockMvc

    private val mapper: ObjectMapper = ObjectMapper().run {
        this.registerModule(KotlinModule())
        this
    }

    @Test
    fun `test of sending complex form to controller, when form is valid`() {
        // Arrange.
        val createForm = TestForm(TestForm.InnerForm("nullable inner form"),
                                  TestForm.InnerForm("not nullable inner form"))
                .let { mapper.writeValueAsString(it) }

        // Act.
        mockMvc.perform(MockMvcRequestBuilders.post(TestControllerMvcITTestConfig.TestController.URI)
                                .header("content-type", "application/json")
                                .content(createForm))

                // Assert.
                .andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    fun `test of sending complex form to controller, when nullable inner form is null`() {
        // Arrange.
        val createForm = TestForm(null,
                                  TestForm.InnerForm("not nullable inner form"))
                .let { mapper.writeValueAsString(it) }

        // Act.
        mockMvc.perform(MockMvcRequestBuilders.post(TestControllerMvcITTestConfig.TestController.URI)
                                .header("content-type", "application/json")
                                .content(createForm))

                // Assert.
                .andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    fun `test of sending complex form to controller, when not nullable inner form is null`() {
        // Arrange.
        val createForm = "{\"nullableForm\":{\"value\":\"nullable inner form\"}}"

        // Act.
        mockMvc.perform(MockMvcRequestBuilders.post(TestControllerMvcITTestConfig.TestController.URI)
                                .header("content-type", "application/json")
                                .content(createForm))

                // Assert.
                .andExpect(MockMvcResultMatchers.status().isBadRequest)
    }

    /**
     * Test configuration for TestControllerMvcIT.
     */
    @TestConfiguration
    open class TestControllerMvcITTestConfig {
        /**
         * Test controller for TestControllerMvcIT.
         */
        @RestController
        open class TestController {
            companion object {
                const val URI = "/test"
            }

            @PostMapping(URI)
            fun test(@RequestBody form: TestForm) {
            }
        }

        class TestForm(val nullableForm: InnerForm?,
                       val notNullableForm: InnerForm) {
            class InnerForm(val value: String)
        }
    }
}
