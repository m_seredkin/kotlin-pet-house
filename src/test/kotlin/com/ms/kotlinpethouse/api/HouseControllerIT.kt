package com.ms.kotlinpethouse.api

import com.antkorwin.junit5integrationtestutils.test.runners.EnableRestTests
import com.antkorwin.junit5integrationtestutils.test.runners.EnableRiderTests
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.github.database.rider.core.api.dataset.DataSet
import com.github.database.rider.core.api.dataset.ExpectedDataSet
import com.ms.kotlinpethouse.api.HouseController.Companion.CREATE_URI
import com.ms.kotlinpethouse.api.HouseController.Companion.GET_URI
import com.ms.kotlinpethouse.api.HouseController.Companion.LIST_URI
import com.ms.kotlinpethouse.api.dto.CollectionDto
import com.ms.kotlinpethouse.api.dto.HouseDto
import com.ms.kotlinpethouse.api.form.HouseCreateForm
import org.assertj.core.api.Assertions
import org.assertj.core.groups.Tuple
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.*
import java.util.function.*

/**
 * @author Maxim_Seredkin
 * @since 6/2/2018
 */
@EnableRestTests
@EnableRiderTests
internal class HouseControllerIT {
    @Autowired
    lateinit var mockMvc: MockMvc

    val mapper: ObjectMapper = ObjectMapper().run {
        this.registerModule(KotlinModule())
        this
    }

    @Test
    @ExpectedDataSet(value = ["/datasets/api/house-controller-it/create__expected.json"])
    fun `test of creating new House`() {
        // Arrange.
        val name = "house name"
        val createForm = HouseCreateForm(name)
                .let { mapper.writeValueAsString(it) }

        // Act.
        val actualResult: HouseDto = mockMvc.perform(post(CREATE_URI)
                                                             .header("content-type", "application/json")
                                                             .content(createForm))

                // Assert.
                .andExpect(status().isCreated)
                .andReturn()
                .response
                .contentAsString
                .let { mapper.readValue(it, HouseDto::class.java) }

        Assertions.assertThat(actualResult)
                .isNotNull
                .extracting(Function<HouseDto, Any> { it.name })
                .containsExactly(name)
    }

    @Test
    @DataSet(value = ["/datasets/api/house-controller-it/get-list.json"],
             cleanBefore = true,
             cleanAfter = true)
    @ExpectedDataSet(value = ["/datasets/api/house-controller-it/get-list.json"])
    fun `test of getting list of Houses`() {
        // Act.
        val actualResult: CollectionDto<HouseDto> = mockMvc.perform(get(LIST_URI))

                // Assert.
                .andExpect(status().isOk)
                .andReturn()
                .response
                .contentAsString
                .let { mapper.readValue(it, object : TypeReference<CollectionDto<HouseDto>>() {}) }

        Assertions.assertThat(actualResult.total)
                .isEqualTo(2L)

        Assertions.assertThat(actualResult.items)
                .extracting(Function<HouseDto, Any> { it.id })
                .containsExactly(Tuple.tuple(UUID.fromString("00000000-0000-0000-0000-000000000000")),
                                 Tuple.tuple(UUID.fromString("00000000-0000-0000-0000-000000000001")))
    }

    @Test
    @DataSet(value = ["/datasets/api/house-controller-it/get.json"],
             cleanBefore = true,
             cleanAfter = true)
    @ExpectedDataSet(value = ["/datasets/api/house-controller-it/get.json"])
    fun `test of getting the House`() {
        // Arrange.
        val id = UUID.fromString("00000000-0000-0000-0000-000000000000")

        // Act.
        val actualResult: HouseDto = mockMvc.perform(get(GET_URI, id))

                // Assert.
                .andExpect(status().isOk)
                .andReturn()
                .response
                .contentAsString
                .let { mapper.readValue(it, HouseDto::class.java) }

        Assertions.assertThat(actualResult)
                .extracting(Function<HouseDto, Any> { it.id },
                            Function<HouseDto, Any> { it.name })
                .containsExactly(id,
                                 "house1")
    }
}