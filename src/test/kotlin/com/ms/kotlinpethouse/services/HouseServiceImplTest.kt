package com.ms.kotlinpethouse.services

import com.ms.kotlinpethouse.models.House
import com.ms.kotlinpethouse.repositories.HouseRepository
import com.ms.kotlinpethouse.services.arguments.HouseCreateArgument
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito
import org.mockito.Mockito.*
import java.util.function.*

/**
 * @author Maxim_Seredkin
 * @since 5/28/2018
 */
internal class HouseServiceImplTest {
    private val houseRepository: HouseRepository = Mockito.mock(HouseRepository::class.java)
    private val houseService: HouseServiceImpl = HouseServiceImpl(houseRepository)

    @Test
    fun `test of creating new House`() {
        // Arrange.
        val name = "house name"

        val expectedResult = mock(House::class.java)

        `when`(houseRepository.save(any(House::class.java))).thenReturn(expectedResult)

        // Act.
        val actualResult = houseService.create(HouseCreateArgument(name))

        // Assert.
        assertThat(actualResult).isSameAs(expectedResult)

        val houseCaptor = ArgumentCaptor.forClass(House::class.java)
        verify(houseRepository).save(houseCaptor.capture())

        assertThat(houseCaptor.value).extracting(Function<House, Any?> { it.id },
                                                 Function<House, Any>(House::name))
                .containsExactly(null, name)
    }
}
