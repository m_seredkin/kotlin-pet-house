package com.ms.kotlinpethouse.services

import com.ms.kotlinpethouse.models.House
import com.ms.kotlinpethouse.services.arguments.HouseCreateArgument
import java.util.*

/**
 * @author Maxim_Seredkin
 * @since 5/28/2018
 */
interface HouseService {
    /**
     * Creates new House.
     */
    fun create(createArgument: HouseCreateArgument): House

    /**
     * Gets all Houses.
     */
    fun getAll(): List<House>

    /**
     * Gets the House.
     */
    fun get(houseId: UUID): House
}