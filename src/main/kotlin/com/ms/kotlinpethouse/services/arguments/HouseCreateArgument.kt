package com.ms.kotlinpethouse.services.arguments

/**
 * @author Maxim_Seredkin
 * @since 5/28/2018
 */
class HouseCreateArgument(val name: String)