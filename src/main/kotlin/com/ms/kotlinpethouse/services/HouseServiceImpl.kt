package com.ms.kotlinpethouse.services

import com.ms.kotlinpethouse.models.House
import com.ms.kotlinpethouse.repositories.HouseRepository
import com.ms.kotlinpethouse.services.arguments.HouseCreateArgument
import javassist.NotFoundException
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

/**
 * @author Maxim_Seredkin
 * @since 5/28/2018
 */
@Service
class HouseServiceImpl(private val houseRepository: HouseRepository) : HouseService {
    @Transactional
    override fun create(createArgument: HouseCreateArgument): House {
        val house = House(name = createArgument.name)
        return houseRepository.save(house)
    }

    @Transactional(readOnly = true)
    override fun getAll(): List<House> {
        return houseRepository.findAll()
    }

    @Transactional(readOnly = true)
    override fun get(houseId: UUID): House {
        return houseRepository.findById(houseId)
                .orElseThrow { NotFoundException("House $houseId is not found.") }
    }
}