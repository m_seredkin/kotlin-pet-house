package com.ms.kotlinpethouse.repositories

import com.ms.kotlinpethouse.models.House
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

/**
 * @author Maxim_Seredkin
 * @since 5/28/2018
 */
@Repository
interface HouseRepository : JpaRepository<House, UUID>