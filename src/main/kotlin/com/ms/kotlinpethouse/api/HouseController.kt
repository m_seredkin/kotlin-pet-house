package com.ms.kotlinpethouse.api

import com.ms.kotlinpethouse.api.dto.CollectionDto
import com.ms.kotlinpethouse.api.dto.HouseDto
import com.ms.kotlinpethouse.api.form.HouseCreateForm
import com.ms.kotlinpethouse.mappers.HouseToHouseDtoMapper
import com.ms.kotlinpethouse.services.HouseService
import com.ms.kotlinpethouse.services.arguments.HouseCreateArgument
import org.springframework.http.HttpStatus.CREATED
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.util.*

/**
 * @author Maxim_Seredkin
 * @since 6/2/2018
 */
@RestController
class HouseController(private val houseService: HouseService,
                      private val houseToHouseDtoMapper: HouseToHouseDtoMapper) {
    companion object {
        private const val BASE_URI = "/houses"
        internal const val CREATE_URI: String = "$BASE_URI/create"
        internal const val LIST_URI: String = "$BASE_URI/list"
        internal const val GET_URI: String = "$BASE_URI/{houseId}"
    }

    @PostMapping(CREATE_URI)
    @ResponseStatus(value = CREATED)
    fun create(@Validated @RequestBody createForm: HouseCreateForm): HouseDto {
        return houseService.create(HouseCreateArgument(createForm.name))
                .run { houseToHouseDtoMapper.map(this) }
    }

    @GetMapping(LIST_URI)
    fun list(): CollectionDto<HouseDto> {
        return houseService.getAll()
                .run { houseToHouseDtoMapper.map(this) }
    }

    @GetMapping(GET_URI)
    fun get(@PathVariable(name = "houseId") houseId: UUID): HouseDto {
        return houseService.get(houseId)
                .run { houseToHouseDtoMapper.map(this) }
    }
}