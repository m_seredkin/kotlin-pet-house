package com.ms.kotlinpethouse.api.dto

import java.util.*

class HouseDto {
    lateinit var id: UUID
    lateinit var name: String
}
