package com.ms.kotlinpethouse.api.dto

/**
 * Created by Maxim Seredkin on 6/2/2018.
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
data class CollectionDto<ItemT>(var items: Collection<ItemT>,
                                var total: Long = 0L)
