package com.ms.kotlinpethouse.api.form

import javax.validation.constraints.NotBlank

data class HouseCreateForm(@field:NotBlank val name: String)
