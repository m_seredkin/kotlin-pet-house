package com.ms.kotlinpethouse

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotlinPetHouseApplication

fun main(args: Array<String>) {
    runApplication<KotlinPetHouseApplication>(*args)
}
