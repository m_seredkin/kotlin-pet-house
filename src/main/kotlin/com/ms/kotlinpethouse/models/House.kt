package com.ms.kotlinpethouse.models

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table
import javax.validation.constraints.NotBlank

/**
 * @author Maxim_Seredkin
 * @since 5/28/2018
 */
@Entity
@Table(name = "HOUSE")
open class House(@Column(name = "NAME")
                 @field:NotBlank var name: String = "") : BaseEntity()