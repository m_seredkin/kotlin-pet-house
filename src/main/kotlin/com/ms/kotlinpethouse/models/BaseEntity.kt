package com.ms.kotlinpethouse.models

import org.hibernate.annotations.Type
import java.io.Serializable
import java.util.*
import javax.persistence.Column
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType.AUTO
import javax.persistence.Id
import javax.persistence.MappedSuperclass

/**
 * @author Maxim_Seredkin
 * @since 5/28/2018
 */
@MappedSuperclass
abstract class BaseEntity(@Id
                          @GeneratedValue(strategy = AUTO)
                          @Column(name = "ID", updatable = false)
                          @Type(type = "uuid-char")
                          var id: UUID? = null) : Serializable {
    val isNew: Boolean
        get() = id != null
}