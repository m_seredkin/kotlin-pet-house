package com.ms.kotlinpethouse.mappers

import com.ms.kotlinpethouse.api.dto.HouseDto
import com.ms.kotlinpethouse.models.House
import org.mapstruct.Mapper

/**
 * Created by Maxim Seredkin on 6/2/2018.
 *
 * Mapper from House to House Dto.
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
@Mapper
abstract class HouseToHouseDtoMapper : BaseMapper<House, HouseDto>()