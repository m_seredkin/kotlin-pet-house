package com.ms.kotlinpethouse.mappers

/**
 * Created by Maxim Seredkin on 6/2/2018.
 *
 * Basically mapper.
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
interface Mapper<FromT, ToT> {
    /**
     * Maps from FromT to ToT.
     */
    fun map(fromT: FromT): ToT

    /**
     * Maps from collection of FromT to collection of ToT.
     */
    fun mapCollection(fromCollection: Collection<FromT>): Collection<ToT>
}