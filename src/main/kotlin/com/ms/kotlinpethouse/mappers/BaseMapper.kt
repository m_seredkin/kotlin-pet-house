package com.ms.kotlinpethouse.mappers

import com.ms.kotlinpethouse.api.dto.CollectionDto
import org.springframework.data.domain.Page

/**
 * Created by Maxim Seredkin on 6/2/2018.
 *
 * Base mapper.
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
abstract class BaseMapper<FromT, ToT> : Mapper<FromT, ToT> {
    /**
     * Maps from collection of FromT to collection dto.
     */
    fun map(fromCollection: Collection<FromT>): CollectionDto<ToT> {
        return CollectionDto(mapCollection(fromCollection), fromCollection.size.toLong())
    }

    /**
     * Maps from page of FromT to collection dto.
     */
    fun map(fromPage: Page<FromT>): CollectionDto<ToT> {
        return CollectionDto(mapCollection(fromPage.content), fromPage.totalElements)
    }
}